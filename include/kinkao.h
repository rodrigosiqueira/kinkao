#ifndef __KINKAO_H__
#define __KINKAO_H__

struct timer {
	unsigned int timeout;
};

struct activity {
	char *name;
	char *description;
	char *note;
};

struct execution_atom {
	struct timer timer_atom;
	struct activity activity_atom;
};

/* Timer handlers */
int parser_timer_input(const char *value, struct timer *timer_atom);
int spawn_activity(struct execution_atom *atom_exec);

/* Alert engine */
int play_sound(const char *alert_path);

#endif
