#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <pulse/simple.h>
#include <pulse/error.h>

#define BUFSIZE 1024

/* The Sample format to use */
static const pa_sample_spec ss = {
	.format = PA_SAMPLE_S16LE,
	.rate = 44100,
	.channels = 2
};

int play_sound(const char *alert_path)
{
	pa_simple *s = NULL;
	int ret = 1;
	int error;
	int fd;

	/* replace STDIN with the specified file if needed */
	fd = open(alert_path, O_RDONLY);
	if (fd < 0) {
		fprintf(stderr, ": open() failed: %s\n", strerror(errno));
		return errno;
	}

	ret = dup2(fd, STDIN_FILENO);
	if (ret < 0) {
		fprintf(stderr, ": dup2() failed: %s\n", strerror(errno));
		goto finish;
	}
	close(fd);

	/* Create a new playback stream */
	s = pa_simple_new(NULL,
			  alert_path,
			  PA_STREAM_PLAYBACK,
			  NULL,
			  "kinkao",
			  &ss,
			  NULL,
			  NULL,
			  &error);
	if (!s) {
		fprintf(stderr, ": pa_simple_new() failed: %s\n", pa_strerror(error));
		goto finish;
	}

	for (;;) {
		uint8_t buf[BUFSIZE];
		ssize_t r;

		r = read(STDIN_FILENO, buf, sizeof(buf));
		if (r == -1)
			goto finish;

		if (r == 0) /* EOF */
			break;

		ret = pa_simple_write(s, buf, sizeof(buf), &error);
		if (ret < 0) {
			fprintf(stderr, ": pa_simple_write() failed: %s\n", pa_strerror(error));
			goto finish;
		}
	}

	/* Make sure that every single sample was played */
	if (pa_simple_drain(s, &error) < 0) {
		fprintf(stderr, ": pa_simple_drain() failed: %s\n", pa_strerror(error));
		goto finish;
	}
	ret = 0;

finish:
	if (s)
		pa_simple_free(s);
	return ret;
}
