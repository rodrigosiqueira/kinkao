#include <stdio.h>
#include <stdlib.h>

#include "kinkao.h"
#include "cli.h"

int main(int argc, char **argv)
{
	struct execution_atom atom_exec;

	parser_command_line(argc, argv, &atom_exec);

	spawn_activity(&atom_exec);

	return 0;
}
