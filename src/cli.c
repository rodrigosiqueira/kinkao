#include <stdio.h>
#include <getopt.h>
#include "cli.h"
#include "kinkao.h"

#define KINKAO_SHORT_OPTION "t:"

static struct option kinkao_options[] = {
	{"timer", required_argument, 0, TIMER_OPTION},
	{0, 0, 0, 0}
};

void parser_command_line(int argc, char **argv, struct execution_atom *atom)
{
	int long_option_index = 0;
	int short_option = 0;
	int ret = 0;

	while (1) {
		long_option_index = 0;
		short_option = getopt_long(argc, argv, KINKAO_SHORT_OPTION,
					   kinkao_options, &long_option_index);
		if (short_option == -1)
			break;

		switch (short_option) {
		case TIMER_OPTION:
		case 't':
			ret = parser_timer_input(optarg, &atom->timer_atom);
			if (ret < 0)
				return;
			break;
		case '?':
			printf("Something weird here\n");
			break;
		case ':':
			printf("Missing value\n");
			break;
		default:
			printf("Ivalid option %o\n", short_option);
			break;
		}
	}

	if (optind < argc) {
		printf("non-option ARGV-elements: ");
		while (optind < argc)
			printf("%s ", argv[optind++]);
		printf("\n");
		return;
	}
}
