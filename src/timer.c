#include <stdio.h>
#include <regex.h>
#include <errno.h>
#include <string.h>

#include <unistd.h>
#include <sys/types.h>

#include "kinkao.h"

#define TIMER_REGEX "^[0-9]+[m|s|h]$"

static void child_activity(struct execution_atom *atom_exec)
{
	sleep(atom_exec->timer_atom.timeout);
	play_sound("sounds/complete.wav");
}

int spawn_activity(struct execution_atom *atom_exec)
{
	pid_t atom;

	atom = fork();
	if (!atom) {
		child_activity(atom_exec);
		return 0;
	}

	return 0;
}

int parser_timer_input(const char *value, struct timer *timer_atom)
{
	int ret = 0, timer;
	regex_t regex;

	// Check argument format
	ret = regcomp(&regex, TIMER_REGEX, REG_EXTENDED);
	if (ret < 0)
		goto err;

	ret = regexec(&regex, value, 0, NULL, 0);
	if (ret == REG_NOMATCH) {
		ret = EINVAL;
		goto err_arg;
	}

	// Get value
	ret = sscanf(value, "%d", &timer);
	if (ret != 1) {
		ret = EINVAL;
		goto err_arg;
	}

	// Convert time to seconds
	ret = strlen(value);
	switch (value[ret - 1]) {
	case 'm':
		timer = timer * 60;
		break;
	case 'h':
		timer = timer * 3600;
		break;
	default:
		break;
	}

	timer_atom->timeout = timer;
	ret = 0;

err_arg:
	regfree(&regex);
err:
	if (ret)
		fprintf(stderr, "%s\n", strerror(ret));
	return ret;
}
